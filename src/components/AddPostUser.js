import axios from "axios";
import React, {useState, useEffect} from "react";
import {useSelector} from "react-redux";
import {Link} from "react-router-dom";

let userId = {};
const AddPostUser = () => {
  const [caption, setCaption] = useState("");
  const user = useSelector((state) => state.allUsers.users);

  user.map((users) => {
    userId[users.email_id] = users.user_id;
  });
  const [idUser, setIdUser] = useState();
  const [image, setImage] = useState("");

  const getUser = async () => {
    const response = await axios
      .get(`https://reddit-app-api.herokuapp.com/auth_user/`)
      .catch((err) => {
        console.log("error", err);
      });
    let name = response.data[0].user_email;
    console.log(name);
    setIdUser(name);
    console.log(typeof name);
    console.log(idUser);
  };
  useEffect(() => {
    getUser();
  }, []);
  console.log(userId[idUser]);
  const postImage = () => {
    const formData = new FormData();
    formData.append("file", image);
    formData.append("upload_preset", "r97kk9a7");

    axios
      .post("https://api.cloudinary.com/v1_1/dunfngvsv/image/upload", formData)
      .then((response) => {
        console.log(response.data.secure_url);
        const image_url = response.data.secure_url;
        addImage(image_url);
      });
    const addImage = async (url) => {
      const response = await axios.post(
        `https://reddit-app-api.herokuapp.com/users/${userId[idUser]}/users_posts`,
        {
          user_id: `${userId[idUser]}`,
          image_url: `${url}`,
          post_caption: `${caption}`,
        }
      );
      console.log(response);
    };
  };
  return (
    <div className="postAdd">
      <div className="userPosts">
        <input
          className="caption"
          type="text"
          value={caption}
          placeholder="Add a caption"
          onChange={(e) => {
            setCaption(e.target.value);
          }}
        />
        <input
          type="file"
          onChange={(e) => {
            setImage(e.target.files[0]);
          }}
        />
        <Link to="/">
          <button onClick={postImage}>Add</button>
        </Link>
      </div>
    </div>
  );
};

export default AddPostUser;
