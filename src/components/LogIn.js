import React, {useEffect, useState} from "react";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faArrowLeft} from "@fortawesome/free-solid-svg-icons";
import redditLogo from "../Images/reddit_logo.png";
import {Link, useNavigate} from "react-router-dom";
import {useSelector, useDispatch} from "react-redux";
import {
  setLogin,
} from "../redux/actions/usersAction";
import UsersLists from "./UsersLists";
import axios from "axios";
const LogIn = () => {
  const logged = useSelector((state) => state.loggined.payLoad);
  const user = useSelector((users) => users.allUsers.users);
  console.log(logged);
  const dispatch = useDispatch();
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const emailHandler = (e) => {
    setEmail(e.target.value);
  };
  const passwordHandler = (e) => {
    setPassword(e.target.value);
  };

  let emailArray = [];
  let passwordArray = [];
  user.map(users=>{
    emailArray.push(users.email_id)
    passwordArray.push(users.password)

  })

  console.log(emailArray);
  console.log(passwordArray);
  

  const putUser = async () => {
    const response = await axios
      .put(`https://reddit-app-api.herokuapp.com/auth_user/1`, {
        user_email: `${email}`,
      })
      .catch((err) => {
        console.log("error", err);
      });
    console.log(response.data);
  };
  const logInHandler = (e) => {
    if (email === "" || password === "") {
      alert("Fill all fields");
    } else if(!email.includes(emailArray) || !password.includes(passwordArray)){
      alert("add correct credentials")
    } else {
      e.preventDefault();
      putUser();
      dispatch(setLogin(false));
    }
  };
  return (
    <>
      {logged ? (
        <div className="logInPage">
          <div className="logInHeader">
            <Link to="/">
              <FontAwesomeIcon icon={faArrowLeft} />
            </Link>
            <img className="redditLogo" src={redditLogo} alt="icon" />
            <Link to="/signIn">
              <p>SIgn Up</p>
            </Link>
          </div>
          <h1>Log in to Reddit</h1>
          <form className="formLogIn" onSubmit={logInHandler}>
            <div className="logInForm">
              <input
                className="input"
                type="email"
                placeholder="Email"
                value={email}
                onChange={emailHandler}
              />
              <input
                className="input"
                type="password"
                placeholder="Password"
                value={password}
                onChange={passwordHandler}
              />
            </div>
            <div className="forgot">
              <Link to="/forgot">
                <p>Forgot Password</p>
              </Link>
            </div>
            <p>
              By continuing,you agree to our{" "}
              <Link to="/agreement">User Agreement</Link> and
              <Link to="/agreement"> Privacy Policy</Link>
            </p>
            <button className="logInAccount">LogIn</button>
          </form>
          {/* {render} */}
        </div>
      ) : (
        <UsersLists />
      )}
    </>
  );
};

export default LogIn;
