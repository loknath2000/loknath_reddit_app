import React from "react";
import {Link} from "react-router-dom";
import redditStart from "../Images/reddit_start.png"
import DesktopSection from "./DesktopSection";
const Section = () => {
  return (
    <div className="main">
    <div className="section">
      <Link to="/home">
        <h2 className="home">Home</h2>
      </Link>
      <Link to='popular'>
      <h2 className="popular">Popular</h2>
      </Link>
    </div>
    <img className="logo" src={redditStart} alt="start"/>
    <DesktopSection/>
    </div>
  );
};

export default Section;
