import React from "react";
import vote from "../Images/reddit_vote.png";
import {Link} from "react-router-dom";
import {useSelector} from "react-redux";
const Follow = () => {

  const logged = useSelector((state) => state.loggined.payLoad);
  console.log(logged);
  return (
    <>
      {logged ? (
        <div className="follow">
          <h1>Login / signUp to continue</h1>
          <img className="voteImage" src={vote} alt="follow page" />
          <div className="login">
            <Link to="/logIn">
              <button className="logIn">Log in</button>
            </Link>
            <Link to="/signIn">
              <button className="signUp">Sign up</button>
            </Link>
          </div>
        </div>
      ) : (
        <div className="follow">
          <h1>Welcome to Reddit community</h1>
          <div>

          <img className="voteImage" src={vote} alt="follow page" />
          </div>
          <Link to="/">
            <button className="goToFeed"> click here to go to feed section</button>
          </Link>
        </div>
      )}
    </>
  );
};

export default Follow;
