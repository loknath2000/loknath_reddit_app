import React from "react";
import {Link} from "react-router-dom";

const Footer = () => {
  return (
    <div className="footer">
      <Link to="/">
        <i className="fa-solid fa-house"></i>
      </Link>
      <Link to="/compass">
        <i className="fa-solid fa-compass"></i>
      </Link>
      <Link to="/addPost">
        <i className="fa-solid fa-plus"></i>
      </Link>
      <Link to="/chat">
        <i className="fa-solid fa-comment-dots"></i>
      </Link>
      <Link to="/notifications">
        <i className="fa-solid fa-bell"></i>
      </Link>
    </div>
  );
};

export default Footer;
