import React from "react";
import vote from "../Images/reddit_vote.png";
import {Link} from "react-router-dom";
import {useSelector} from "react-redux";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faUserCircle,faPlusSquare} from "@fortawesome/free-solid-svg-icons";
const AddPost = () => {
  const logged = useSelector((state) => state.loggined.payLoad);
  console.log(logged);
  return (
    <>
      {logged ? (
        <div className="addPosts">
          <div className="add">
            <img className="voteImage" src={vote} alt="vote" />
            <p> Sign up to upvote the best content</p>
            <div className="login">
              <Link to="/logIn">
                <button className="logIn">Log in</button>
              </Link>
              <Link to="/signIn">
                <button className="signUp">Sign up</button>
              </Link>
            </div>
          </div>
        </div>
      ) : (
        <div className="addPost">
          <div className="addHeader">
          <Link to="/">

            <button>X</button>
          </Link>
            <h3>Post to</h3>
          </div>

          <div className="search">
            <i className="fa-solid fa-magnifying-glass"></i>
            <input type="text" placeholder="Search" />
          </div>
          <Link to="/addPostUser">

          <div className="postAdding">
          <FontAwesomeIcon icon={faPlusSquare}/>
          <h3>Add a post</h3>
          </div>
          </Link>

          <div className="profile">
            <FontAwesomeIcon icon={faUserCircle} />
            <h4>My profile</h4>
          </div>
          <button className="avatarPost">Create Avatar</button>
        </div>
      )}
    </>
  );
};

export default AddPost;
