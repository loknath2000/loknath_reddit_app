import React from "react";
import axios from "axios";
import {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {setUsers} from "../redux/actions/usersAction";
import PostsInformation from "./PostsInformation";

const UsersLists = () => {
  const dispatch = useDispatch();

  const fetchUsers = async () => {
    const response = await axios
      .get(`https://reddit-app-api.herokuapp.com/users`)
      .catch((err) => {
        console.log(err);
      });
    console.log("ggggggggg", response.data);
    dispatch(setUsers(response.data));
  };

  const users = useSelector((state) => state.allUsers);
  console.log(users);

  useEffect(() => {
    fetchUsers();
  }, []);
  return (
    <div className="userListsData">
      <PostsInformation />
    </div>
  );
};

export default UsersLists;
