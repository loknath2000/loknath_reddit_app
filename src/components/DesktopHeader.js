import React from "react";
import logo from "../Images/reddit_desktop.jpg";
import {Link} from "react-router-dom";
import {useSelector} from "react-redux";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faPlusSquare} from "@fortawesome/free-solid-svg-icons";
const DesktopHeader = () => {
  const logged = useSelector((state) => state.loggined.payLoad);
  console.log(logged);
  return (
    <>
      {logged ? (
        <div className="desktopHeader">
          <Link to="/follow">
            <img className="desktopLogo" src={logo} alt="reddit" />
          </Link>
          <div className="search">
            <i className="fa-solid fa-magnifying-glass"></i>
            <input type="text" placeholder="Search Reddit" />
          </div>
          <div className="login">
            <Link to="/logIn">
              <button className="logIn">Log in</button>
            </Link>
            <Link to="/signIn">
              <button className="signUp">Sign up</button>
            </Link>
          </div>
          <Link to="user">
            <i className="fa-solid fa-user"></i>
          </Link>
        </div>
      ) : (
        <div className="desktopHeader">
          <Link to="/follow">
            <img className="desktopLogo" src={logo} alt="reddit" />
          </Link>
          <div className="search">
            <i className="fa-solid fa-magnifying-glass"></i>
            <input type="text" placeholder="Search Reddit" />
          </div>
          <Link to="/addPostUser">
          <div className="addDesktopPost">
          <p>Add a post</p>
            <FontAwesomeIcon className="desktopPost" icon={faPlusSquare} />
          </div>
          </Link>
          <Link to="user">
            <i className="fa-solid fa-user"></i>
          </Link>
        </div>
      )}
    </>
  );
};

export default DesktopHeader;
