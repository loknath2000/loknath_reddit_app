import React from "react";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {
  faSignal,
  faUserCircle,
  faAngleRight,
} from "@fortawesome/free-solid-svg-icons";
import {Link} from "react-router-dom";
import {useSelector} from "react-redux";
const Hamburger = () => {
  const logged = useSelector((state) => state.loggined.payLoad);
  console.log(logged);
  return (
    <>
      {logged ? (
        <div className="hamburger">
          <div className="hamMenu">
            <Link to="/">
              <button>Go to Home</button>
            </Link>
            <div className="all">
              <FontAwesomeIcon icon={faSignal}></FontAwesomeIcon>
              <p>All</p>
            </div>
            <Link to="/home">
              <div className="loginPage">
                <FontAwesomeIcon icon={faUserCircle}></FontAwesomeIcon>
                <p>Login in to add your communities</p>
              </div>
            </Link>
          </div>
        </div>
      ) : (
        <div className="hamburger">
          <div className="hamMenu">
            <Link to="/">
              <button>Go to Home</button>
            </Link>
            <div className="angle">
              <h2>Moderating</h2>
              <FontAwesomeIcon icon={faAngleRight} />
            </div>
            <div className="angle">
              <h2>Your communities</h2>
              <FontAwesomeIcon icon={faAngleRight} />
            </div>
            <div className="all">
              <FontAwesomeIcon icon={faSignal}></FontAwesomeIcon>
              <p>Browse communities</p>
            </div>
            <div className="all">
              <FontAwesomeIcon icon={faSignal}></FontAwesomeIcon>
              <p>All</p>
            </div>
          </div>
        </div>
      )}
    </>
  );
};

export default Hamburger;
