import React from "react";
import redditLogo from "../Images/reddit_logo.png";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faArrowLeft} from "@fortawesome/free-solid-svg-icons";
import axios from "axios";
import {useState} from "react";
import {Link} from "react-router-dom";
const SignUp = () => {
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const emailHandler = (e) => {
    setEmail(e.target.value);
  };
  const nameHandler = (e) => {
    setName(e.target.value);
  };
  const passwordHandler = (e) => {
    setPassword(e.target.value);
  };

  const addUser = async () => {
    const response = await axios.post(
      "https://reddit-app-api.herokuapp.com/users",
      {
        name: `${name}`,
        email_id: `${email}`,
        password: `${password}`,
      }
    );
    console.log(email);
    console.log(password);
    alert("Account created successfully");
  };
  const submitHandler = (e) => {
    if (name === "" || email==="" || password === "") {
      alert("Fill all fields");
    } else {
      e.preventDefault();
      addUser();
      setEmail("");
      setName("");
      setPassword("");
    }
  };
  return (
    <div className="signUpPage">
      <div className="signHeader">
        <Link to="/">
          <FontAwesomeIcon icon={faArrowLeft}></FontAwesomeIcon>
        </Link>
        <img className="redditLogo" src={redditLogo} alt="logo" />
        <Link to="/logIn">
          <p>Login</p>
        </Link>
      </div>
      <h1>Hi new friend, welcome to Reddit</h1>
      <form className="signUpForm" onSubmit={submitHandler}>
        <input
          className="input"
          type="email"
          placeholder="Email"
          value={email}
          onChange={emailHandler}
        />

        <input
          className="input"
          type="text"
          placeholder="Username"
          value={name}
          onChange={nameHandler}
        />
        <input
          className="input"
          type="password"
          placeholder="Password"
          value={password}
          onChange={passwordHandler}
        />
        <p>
          By continuing,you agree to our{" "}
          <Link to="/agreement">User Agreement</Link> and
          <Link to="/agreement"> Privacy Policy</Link>
        </p>
        <button className="signUpAccount" type="submit">
          SignUp
        </button>
      </form>
    </div>
  );
};

export default SignUp;
