import React from "react";
import {useSelector} from "react-redux";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {
  faUserCircle,
  faArrowDown,
  faArrowUp,
  faShareNodes,
  faMessage,
  faGift,
  faPlusCircle,
  faEllipsisVertical,
} from "@fortawesome/free-solid-svg-icons";
import {useNavigate} from "react-router";
import {Link} from "react-router-dom";
const UsersInformation = () => {
  const user = useSelector((users) => users.allUsers.users);
  console.log(user);
  const post = useSelector((posts) => Object.values(posts.allPosts));
  console.log(post);
  let name = {};
  user.map((users) => {
    name[users.user_id] = users.name;
  });
  let navigate = useNavigate();
  return (
    <div>
      {post.map((posts) => {
        const {post_id, user_id, image_url, post_caption} = posts;
        return (
          <>
            <div className="usersList">
              <h3 className="userNameHeading">
                <div className="head">
                  <FontAwesomeIcon className="icon" icon={faUserCircle} />
                  <p>{name[user_id]}</p>
                </div>
                <div className="head">
                  <Link to="/follow">
                    <FontAwesomeIcon className="iconPlus" icon={faPlusCircle} />
                  </Link>
                  <FontAwesomeIcon
                    className="iconDots"
                    icon={faEllipsisVertical}
                  />
                </div>
              </h3>
              <h2 className="userName">{post_caption}</h2>
              <img src={image_url} alt="post_image" />
              <div className="comments">
                <div>
                  <FontAwesomeIcon icon={faArrowUp} />
                  <span>Likes</span>
                  <FontAwesomeIcon icon={faArrowDown} />
                </div>
                {/* <Link to="/allComments"> */}
                <div
                  className="allComments"
                  onClick={() => {
                    navigate(`/users_posts/${post_id}/comments`);
                  }}
                >
                  <FontAwesomeIcon icon={faMessage} />
                  <span>Comments</span>
                </div>
                {/* </Link> */}
                <div>
                  <FontAwesomeIcon icon={faShareNodes} />
                  <span>Share</span>
                </div>
                <FontAwesomeIcon icon={faGift} />
              </div>
            </div>
          </>
        );
      })}
    </div>
  );
};

export default UsersInformation;
