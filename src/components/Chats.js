import React from "react";
import vote from "../Images/reddit_vote.png";
import chatLogin from "../Images/chat_login.png";
import {Link} from "react-router-dom";
import {useSelector} from "react-redux";
const Chats = () => {
  const logged = useSelector((state) => state.loggined.payLoad);
  console.log(logged);
  return (
    <>
      {logged ? (
        <div className="chat">
          <div className="add">
            <img className="voteImage" src={vote} alt="vote" />
            <p> Sign up to Chat</p>
            <div className="login">
              <Link to="/logIn">
                <button className="logIn">Log in</button>
              </Link>
              <Link to="/signIn">
                <button className="signUp">Sign up</button>
              </Link>
            </div>
          </div>
        </div>
      ) : (
        <div className="chat">
          <div className="add">
            <img className="voteImage" src={chatLogin} alt="vote" />
            <h3>Your chats will show up here</h3>
            <p className="chatMesage">
              {" "}
              Get Started by tapping the new chat button here or on the
              someone's profile
            </p>
            <Link to="/chatFriend">
              <button className="chatting">Start Chatting</button>
            </Link>
          </div>
        </div>
      )}
    </>
  );
};

export default Chats;
