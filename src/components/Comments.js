import React, {useEffect, useState} from "react";
import {useSelector} from "react-redux";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {
  faUserCircle,
  faPlusCircle,
  faEllipsisVertical,
  faArrowUp,
  faArrowDown,
  faMessage,
} from "@fortawesome/free-solid-svg-icons";
import {Link} from "react-router-dom";
import {useParams} from "react-router";
import axios from "axios";

const Comments = () => {
  const [image, setImage] = useState("");
  const [addComments, setAddComments] = useState("");
  const [getComments, setGetComments] = useState([]);
  const params = useParams();
  const commentsToStore = [];
  const user = useSelector((users) => users.allUsers.users);
  // console.log(user);
  const post = useSelector((posts) => Object.values(posts.allPosts));
  // console.log(post);
  let name = {};
  let userId;
  user.map((users) => {
    name[users.user_id] = users.name;
  });
  post.map((posts) => {
    // console.log(posts.post_id)
    // console.log(params.post_id)
    if (posts.post_id == params.post_id) {
      // console.log(posts.user_id)
      userId = posts.user_id;
      console.log(userId);
    }
  });

  const submitHandler = (e) => {
    e.preventDefault();
    postComment();
    // setAddComments("")
  };

  const selectedPost = async () => {
    const response = await axios
      .get(`https://reddit-app-api.herokuapp.com/users_posts/${params.post_id}`)
      .catch((err) => {
        console.log("error", err);
      });
    setImage(response.data[0].image_url);
    // console.log(response.data);
    selectedPostComments();
  };
  const selectedPostComments = async () => {
    const response = await axios
      .get(
        `https://reddit-app-api.herokuapp.com/users_posts/${params.post_id}/comments`
      )
      .catch((err) => {
        console.log("error", err);
      });
    response.data.map((each) => {
      commentsToStore.push(each.comment);
      // console.log(each.comment)
      setGetComments(commentsToStore);
    });
    // console.log(response.data)
  };
  const postComment = async () => {
    const response = await axios
      .post(
        `https://reddit-app-api.herokuapp.com/users_posts/${params.post_id}/comments`,
        {
          post_id: `${params.post_id}`,
          comment: `${addComments}`,
        }
      )
      .catch((err) => {
        console.log("error", err);
      });
    console.log(response);
  };

  selectedPost();

  const comments = getComments.map((each) => {
    return (
      <div className="comment">
        <p>
        {each}
        </p>
        <div className="reply">
        <FontAwesomeIcon icon={faArrowUp} />
        replies
        <FontAwesomeIcon icon={faArrowDown} />
        <div>
        <FontAwesomeIcon icon={faMessage}/>reply
        </div>
        </div>
      </div>
    );
  });

  return (
    <div className="cs">
      <div className="commentsSection">
        <h3 className="userNameHeading">
          <div className="head">
            <FontAwesomeIcon className="icon" icon={faUserCircle} />
            <p>{name[userId]}</p>
          </div>
          <div className="head">
            <Link to="/follow">
              <FontAwesomeIcon className="iconPlus" icon={faPlusCircle} />
            </Link>
            <FontAwesomeIcon className="iconDots" icon={faEllipsisVertical} />
          </div>
        </h3>
        <img src={image} alt="imgs" />
        <form onSubmit={submitHandler}>
          <input
            className="addComments"
            type="text"
            placeholder="Add ur comments"
            onChange={(e) => {
              setAddComments(e.target.value);
            }}
          />
          <button className="ac" type="submit">
            Add Comment
          </button>
        </form>
        <div className="commentsData">
          All comments
          {comments}
        </div>
      </div>
    </div>
  );
};

export default Comments;
