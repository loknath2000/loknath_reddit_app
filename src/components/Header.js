import React from "react";
import {Link} from "react-router-dom";

const Header = () => {
  return (
    <div className="header">
      <Link to="/hamburger">
        <i className="fa-solid fa-bars"></i>
      </Link>
      <div className="search">
        <i className="fa-solid fa-magnifying-glass"></i>
        <input type="text" placeholder="Search" />
      </div>
      <Link to="user">
        <i className="fa-solid fa-user"></i>
      </Link>
    </div>
  );
};

export default Header;
