import React,{useEffect} from 'react';
import pic1 from '../Images/reddit_icon.jpg'
import { Link } from 'react-router-dom';
import { useSelector } from 'react-redux';
import UsersLists from './UsersLists';
import { setUser } from '../redux/actions/usersAction';
import { useDispatch } from 'react-redux';
import axios from "axios"

const Home = () => {
    const logged = useSelector((state) => state.loggined.payLoad);
    console.log(logged);
    // const dispatch = useDispatch()
    // const fetchActiveUser = async () => {
    //     const response = await axios.get(`https://reddit-app-api.herokuapp.com/users/state/0`)
    //         .catch((err) => {
    //             console.log("error", err)
    //         })
    //     console.log(response.data)
    //     dispatch(setUser(response.data))
    // }
    // useEffect(()=>{
    //   fetchActiveUser()
    // },[])

    return (
        <>
          { logged ?
        <div className='home'>
        <img className="welcomeIcon" src={pic1} alt="icon" />
        <div className='welcome'>
            <h2>Welcome!</h2>
            <p> There's a Reddit community for every topic imaginable</p>
        </div>
        <div className='login'>
        <Link to="/logIn">
        <button className='logIn'>
            Log in
        </button>
        </Link>
        <Link to="/signIn">
        <button className='signUp'>
            Sign up
        </button>
        </Link>
        </div>
        </div>:<UsersLists/>
          }  
        </>
    );
};

export default Home;