import React, {useEffect, useState} from "react";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {
  faUser,
  faUserCircle,
  faGear,
  faArrowLeft,
  faDotCircle,
  faCoins,
  faShieldAlt,
  faIdBadge,
} from "@fortawesome/free-solid-svg-icons";
import {Link} from "react-router-dom";
import {useSelector, useDispatch} from "react-redux";
import {setLogin} from "../redux/actions/usersAction";
import axios from "axios";
// let userName
let usersList = {};
const User = () => {
  const [userName, setUserName] = useState();
  const user = useSelector((state) => state.allUsers.users);
  console.log(user);
  const logged = useSelector((state) => state.loggined.payLoad);
  console.log(logged);
  const dispatch = useDispatch();

  user.map((users) => {
    usersList[users.email_id] = users.name;
    console.log(usersList);
  });

  console.log(usersList["hello@email.com"]);

  const getUser = async () => {
    const response = await axios
      .get(`https://reddit-app-api.herokuapp.com/auth_user/`)
      .catch((err) => {
        console.log("error", err);
      });
    let name = response.data[0].user_email;
    console.log(name);
    setUserName(name);
    console.log(typeof name);
    console.log(userName);
  };
  useEffect(() => {
    getUser();
  }, []);
  console.log(usersList[userName]);

  const logOutHandler = (e) => {
    e.preventDefault();
    dispatch(setLogin(true));
  };
  return (
    <>
      {logged ? (
        <div className="users">
          <div className="details">
            <FontAwesomeIcon
              className="userPic"
              icon={faUser}
            ></FontAwesomeIcon>
            <p>
              {" "}
              Sing up to upvote the best content customize your feed, share your
              interests, and more!
            </p>
            <Link to="/home">
              <div className="sign">
                <FontAwesomeIcon icon={faUserCircle}></FontAwesomeIcon>
                <h4>Sign up / Log in</h4>
              </div>
            </Link>
            <div className="settings">
              <FontAwesomeIcon icon={faGear}></FontAwesomeIcon>
              <h4>Settings</h4>
            </div>
            <Link to="/">
              <div className="goBack">
                <FontAwesomeIcon icon={faArrowLeft}></FontAwesomeIcon>
                <h4>Go back</h4>
              </div>
            </Link>
          </div>
        </div>
      ) : (
        <div className="users">
          <div className="details">
            <h1>user profile</h1>
            <FontAwesomeIcon
              className="userPic"
              icon={faUser}
            ></FontAwesomeIcon>
            <div>
              <h3>{usersList[userName]}</h3>
              <button className="onlineStatus">
                <FontAwesomeIcon icon={faDotCircle} /> Online Status: on{" "}
              </button>
            </div>
            <div>
              <button className="avatar">
                Create Avatar
                {/* <FontAwesomeIcon icon={faAngleRight} /> */}
              </button>
            </div>
            <div className="profile">
              <FontAwesomeIcon icon={faUserCircle} />
              <h4>My profile</h4>
            </div>
            <div className="coins">
              <FontAwesomeIcon icon={faCoins} />
              <h4> Reddit Coins</h4>
            </div>
            <div className="premium">
              <FontAwesomeIcon icon={faShieldAlt} />
              <h4>Reddit Premium</h4>
            </div>
            <div className="saved">
              <FontAwesomeIcon icon={faIdBadge} />
              <h4>Saved</h4>
            </div>
            <div className="settings">
              <FontAwesomeIcon icon={faGear}></FontAwesomeIcon>
              <h4>Settings</h4>
            </div>
            <Link to="/">
              <div className="goBack">
                <FontAwesomeIcon icon={faArrowLeft}></FontAwesomeIcon>
                <h4>Go back</h4>
              </div>
            </Link>
            <button className="logOut" onClick={logOutHandler}>
              Log Out
            </button>
          </div>
        </div>
      )}
    </>
  );
};

export default User;
