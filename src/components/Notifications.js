import React from "react";
import {Link} from "react-router-dom";
import {useSelector} from "react-redux";
const Notifications = () => {
  const logged = useSelector((state) => state.loggined.payLoad);
  console.log(logged);
  return (
    <>
      {logged ? (
        <div className="notify">
          <div className="headerMessages">
            {/* <Link to="/default"> */}
            <h4>Notifications</h4>
            {/* </Link> */}
            <h4>Messages</h4>
          </div>
          <div className="tooMuchInfo">
            <h5>Too much info? Not enough info?</h5>
            <p>
              Sign Up or Log in to customize your notifications, including chats
              and inbox
            </p>
            <div className="login">
              <Link to="/logIn">
                <button className="logIn">Log in</button>
              </Link>
              <Link to="/signIn">
                <button className="signUp">Sign up</button>
              </Link>
            </div>
          </div>
        </div>
      ) : (
        <div className="notify">
          <div className="headerMessages">
            {/* <Link to="/default"> */}
            <h4>Notifications</h4>
            {/* </Link> */}
            <h4>Messages</h4>
          </div>
          <div className="notificationsBody"></div>
        </div>
      )}
    </>
  );
};

export default Notifications;
