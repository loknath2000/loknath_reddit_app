import "./App.css";
import Header from "./components/Header";
import Section from "./components/Section";
import Footer from "./components/Footer";
import Compass from "./components/Compass";
import Home from "./components/Home";
// import Popular from "./components/Popular";
import UserLists from "./components/UsersLists"
import Hamburger from "./components/Hamburger";
import User from "./components/User";
import {BrowserRouter, Routes, Route} from "react-router-dom";
import SignUp from "./components/SignUp";
import LogIn from "./components/LogIn";
import AddPost from "./components/AddPost";
import Chats from "./components/Chats"
import Notifications from "./components/Notifications";
import DesktopHeader from "./components/DesktopHeader";
import DesktopSection from "./components/DesktopSection";
import Follow from "./components/Follow";
import Comments from "./components/Comments";
import AddPostUser from "./components/AddPostUser";
function App() {
  return (
    <div className="App">
      <BrowserRouter>
      <Header />
      <DesktopHeader/>
      {/* <DesktopSection/> */}
        <Routes>
        <Route path="/logIn"  element={<LogIn/>}/>
        <Route path="/signIn" element={<SignUp/>}/>
        <Route path="/hamburger" element={<Hamburger/>}/>
        <Route path="/user" element={<User/>}/>
        <Route path="/follow" element={<Follow/>}/>
          <Route path="/"  element={<Section />} />
          <Route path="/home" element={<Home />}/>
          <Route path="/compass"  element={<UserLists />}/>
          <Route path="/popular"  element={<UserLists/>}/>
          <Route path="/addPost" element={<AddPost/>}/>
          <Route path="/chat" element={<Chats/>}/>
          <Route path="/notifications" element={<Notifications/>}/>
          <Route path="/users_posts/:post_id/comments" element={<Comments/>}/>
          <Route path="/addPostUser" element={<AddPostUser/>}/>
        </Routes>
      <div className="cont"></div>
      <Footer />
      </BrowserRouter>
    </div>
  );
}

export default App;
