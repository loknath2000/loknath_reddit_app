import {combineReducers} from "redux";
import {
  usersReducer,
  selectedUsersReducer,
  usersPostsReducer,
  loginReducer,
  nameReducer,
  passwordReducer,
  userReducer,
} from "./usersReducer";

const reducers = combineReducers({
  allUsers: usersReducer,
  allPosts: usersPostsReducer,
  User: selectedUsersReducer,
  loggined:loginReducer,
  name:nameReducer,
  password:passwordReducer,
  particularuser:userReducer,
});

export default reducers;
