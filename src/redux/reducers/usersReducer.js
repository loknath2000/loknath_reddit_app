

const initialState = {
  users: [],
};

export const usersReducer = (user = initialState, {type, payLoad}) => {
  switch (type) {
    case "SET_USERS":
      return {...user, users: payLoad}; //assigning empty states array to Payload that is url from setlists.js
    default:
      return user;
  }
};

export const selectedUsersReducer = (user = {}, {type, payLoad}) => {
  switch (type) {
    case "SELECTED_USERS":
      return {...user, ...payLoad};
    case "REMOVE_SELECTED_PRODUCT":
      return {};
    default:
      return user;
  }
};

export const usersPostsReducer = (post = {}, {type, payLoad}) => {
  switch (type) {
    case "SET_POSTS":
      return {...post, ...payLoad};
    default:
      return post;
  }
};


export const loginReducer = (post=true,{type,payLoad})=>{
  switch (type) {
    case "SET_LOGIN":
      return {...post,payLoad}
      default:
        return post;
  }
}


export const nameReducer = (post="",{type,payLoad})=>{
  switch (type) {
    case "SET_NAME":
      return {post,payLoad}
      default:
        return post;
  }
}
export const passwordReducer = (post="",{type,payLoad})=>{
  switch (type) {
    case "SET_PASSWORD":
      return {...post,payLoad}
      default:
        return post;
  }
}

export const userReducer = (state = initialState, { type, payload }) => {
    switch (type) {
        case "SET_USER":
            return { ...state, user: payload }
        default:
            return state
    }
}

