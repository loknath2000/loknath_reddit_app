export const setUsers = (users) => {
  return {
    type: "SET_USERS",
    payLoad: users,
  };
};

export const selectedPosts = (post) => {
  return {
    type: "SELECTED_POSTS",
    payLoad: post,
  };
};

export const removeSelectedPosts = () => {
  return {
    type: "REMOVE_SELECTED_POST",
  };
};

export const setPosts = (post) => {
  return {
    type: "SET_POSTS",
    payLoad: post,
  };
};
export const setLogin = (post) =>{
  return {
    type:"SET_LOGIN",
    payLoad:post,
  }
}
export const setNameAction = (post) =>{
  return {
    type:"SET_NAME",
    payLoad:post,
  }
}
export const setPasswordAction = (post) =>{
  return {
    type:"SET_PASSWORD",
    payLoad:post,
  }
}
export const setUser = (user) => {
    return {
        type: "SET_USER",
        payload: user,
    }
}